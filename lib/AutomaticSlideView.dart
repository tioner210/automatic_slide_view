import 'dart:async';

import 'package:flutter/material.dart';

class AutomaticSlideView extends StatefulWidget {
  final List<Widget> views;
  final Duration durationToChange;
  final Duration animatedDuration;
  final Widget spacer;
  final Curve animatedCurve;

  final Widget? leading;

  final Widget? trailing;

  final Color? selectedColor;

  final Color? unselectedColor;
  final EdgeInsetsGeometry? margin;
  AutomaticSlideView({
    Key? key,
    required this.views,
    this.margin,
    this.durationToChange = const Duration(seconds: 5),
    this.animatedDuration = const Duration(seconds: 4),
    this.spacer = const Spacer(),
    this.animatedCurve = Curves.linear,
    this.leading,
    this.trailing,
    this.selectedColor,
    this.unselectedColor,
  }) : super(key: key) {
    assert(views.isNotEmpty);
  }

  @override
  _AutomaticSlideViewState createState() => _AutomaticSlideViewState();
}

class _AutomaticSlideViewState extends State<AutomaticSlideView> {
  int page = 0;

  final _pageContoller = PageController();

  late Timer _timer;

  @override
  void initState() {
    _timer = Timer.periodic(widget.durationToChange, (t) {
      page = (page + 1) % widget.views.length;
      if (_pageContoller.position.hasPixels) {
        _pageContoller.animateToPage(page,
            duration: widget.animatedDuration, curve: widget.animatedCurve);
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    _pageContoller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _barIndiator = widget.views
        .map((e) => Punto(
            seleccionado: page == widget.views.indexOf(e),
            selectedColor: widget.selectedColor ?? Theme.of(context).focusColor,
            unselectedColor: widget.unselectedColor ?? const Color(0xFFe5e5e5)))
        .toList();

    _barIndiator = _barIndiator.expand((element) {
      if (_barIndiator.indexOf(element) < _barIndiator.length - 1) {
        return [
          element,
          widget.spacer,
        ];
      } else {
        return [element];
      }
    }).toList();

    if (widget.leading != null) {
      _barIndiator.insert(0, const Spacer());
      _barIndiator.insert(0, widget.leading!);
    }
    if (widget.trailing != null) {
      _barIndiator.add(widget.trailing!);
      EdgeInsets.symmetric(
        vertical: MediaQuery.of(context).size.height * .05,
        horizontal: MediaQuery.of(context).size.width * .1,
      );
    }

    return Container(
      margin: widget.margin ??
          EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.height * .05,
            horizontal: MediaQuery.of(context).size.width * .1,
          ),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            flex: 25,
            child: PageView(
              controller: _pageContoller,
              onPageChanged: (value) {
                setState(() {
                  page = value;
                });
              },
              children: widget.views,
            ),
          ),
          Expanded(
            flex: 2,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: _barIndiator,
            ),
          )
        ],
      ),
    );
  }
}

class Punto extends StatelessWidget {
  final bool seleccionado;

  final Color selectedColor;
  final Color unselectedColor;
  const Punto({
    Key? key,
    required this.seleccionado,
    required this.selectedColor,
    required this.unselectedColor,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: const Duration(seconds: 2),
      child: seleccionado
          ? Container(
              width: MediaQuery.of(context).size.width * .09,
              height: MediaQuery.of(context).size.width * .03,
              decoration: BoxDecoration(
                  color: seleccionado ? selectedColor : unselectedColor,
                  borderRadius: BorderRadius.circular(10)),
              child: const SizedBox(),
            )
          : Container(
              width: MediaQuery.of(context).size.width * .03,
              height: MediaQuery.of(context).size.width * .03,
              decoration: BoxDecoration(
                  color: seleccionado ? selectedColor : unselectedColor,
                  borderRadius: BorderRadius.circular(10)),
              child: const SizedBox(),
            ),
    );
  }
}
