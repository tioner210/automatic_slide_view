import 'package:automatic_slide_view/AutomaticSlideView.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            flex: 7,
            child: AutomaticSlideView(
              selectedColor: Colors.red,
              trailing: const Spacer(flex: 10),
              views: const [
                Text("data"),
                Text("data"),
                Text("data"),
              ],
            ),
          ),
          Spacer()
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
